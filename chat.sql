/*
 Navicat Premium Data Transfer

 Source Server         : 【LocalHost】
 Source Server Type    : MySQL
 Source Server Version : 100410
 Source Host           : localhost:3306
 Source Schema         : chat

 Target Server Type    : MySQL
 Target Server Version : 100410
 File Encoding         : 65001

 Date: 13/11/2023 12:36:05
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for chat_content
-- ----------------------------
DROP TABLE IF EXISTS `chat_content`;
CREATE TABLE `chat_content`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NULL DEFAULT NULL,
  `user_name` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `to_id` int(10) UNSIGNED NULL DEFAULT 0,
  `to_name` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `create_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 155 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for chat_user
-- ----------------------------
DROP TABLE IF EXISTS `chat_user`;
CREATE TABLE `chat_user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `fd` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '推送ID',
  `user_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '用户名',
  `avatar` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '头像',
  `online_status` tinyint(3) UNSIGNED NOT NULL DEFAULT 1 COMMENT '在线状态：0离线，1在线',
  `add_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '注册时间',
  `update_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `user_name`(`user_name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 752 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for yi_chat
-- ----------------------------
DROP TABLE IF EXISTS `yi_chat`;
CREATE TABLE `yi_chat`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NULL DEFAULT NULL,
  `user_name` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `to_id` int(10) UNSIGNED NULL DEFAULT 0,
  `to_name` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `create_at` datetime(0) NULL DEFAULT NULL,
  `is_push` tinyint(1) UNSIGNED NULL DEFAULT 0,
  `is_read` tinyint(1) UNSIGNED NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 56 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '个人聊天' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for yi_chat_group
-- ----------------------------
DROP TABLE IF EXISTS `yi_chat_group`;
CREATE TABLE `yi_chat_group`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `group_id` int(10) UNSIGNED NULL DEFAULT NULL,
  `group_name` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `user_id` int(10) UNSIGNED NULL DEFAULT NULL,
  `user_name` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `create_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 27 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '群组聊天' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for yi_friend
-- ----------------------------
DROP TABLE IF EXISTS `yi_friend`;
CREATE TABLE `yi_friend`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `friend_id` int(10) UNSIGNED NOT NULL,
  `friend_group_id` int(10) UNSIGNED NULL DEFAULT NULL,
  `status` tinyint(1) UNSIGNED NULL DEFAULT 0 COMMENT '0：正常 1：已删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 52 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '朋友' ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of yi_friend
-- ----------------------------
INSERT INTO `yi_friend` VALUES (1, 100040, 100020, 1, 0);
INSERT INTO `yi_friend` VALUES (2, 100040, 100022, 1, 0);
INSERT INTO `yi_friend` VALUES (3, 100040, 100024, 2, 0);
INSERT INTO `yi_friend` VALUES (4, 100040, 100025, 3, 0);
INSERT INTO `yi_friend` VALUES (5, 100040, 100027, 2, 0);
INSERT INTO `yi_friend` VALUES (47, 100048, 100001, 0, 0);
INSERT INTO `yi_friend` VALUES (46, 100001, 100048, 0, 0);
INSERT INTO `yi_friend` VALUES (45, 100048, 100000, 0, 0);
INSERT INTO `yi_friend` VALUES (44, 100000, 100048, 0, 0);
INSERT INTO `yi_friend` VALUES (51, 100044, 100048, 10000, 0);
INSERT INTO `yi_friend` VALUES (50, 100048, 100044, 10000, 0);
INSERT INTO `yi_friend` VALUES (49, 100047, 100048, 0, 0);
INSERT INTO `yi_friend` VALUES (48, 100048, 100047, 0, 0);

-- ----------------------------
-- Table structure for yi_friend_group
-- ----------------------------
DROP TABLE IF EXISTS `yi_friend_group`;
CREATE TABLE `yi_friend_group`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NULL DEFAULT NULL,
  `groupname` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '朋友分组' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yi_friend_group
-- ----------------------------
INSERT INTO `yi_friend_group` VALUES (1, 100040, 'A组');
INSERT INTO `yi_friend_group` VALUES (2, 100040, 'B组');
INSERT INTO `yi_friend_group` VALUES (3, 100040, 'C组');

-- ----------------------------
-- Table structure for yi_group
-- ----------------------------
DROP TABLE IF EXISTS `yi_group`;
CREATE TABLE `yi_group`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NULL DEFAULT NULL,
  `groupname` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `avatar` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `status` tinyint(1) UNSIGNED NULL DEFAULT 0 COMMENT '0正常  1已删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 10003 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '群组' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yi_group
-- ----------------------------
INSERT INTO `yi_group` VALUES (10000, 100048, '神蛊峰', '/upload/header/h36.jpg', 0);
INSERT INTO `yi_group` VALUES (10001, 100048, '天允山', '/upload/header/h34.jpg', 0);
INSERT INTO `yi_group` VALUES (10002, 100048, '黑水城', '/upload/header/h20.jpg', 0);

-- ----------------------------
-- Table structure for yi_group_member
-- ----------------------------
DROP TABLE IF EXISTS `yi_group_member`;
CREATE TABLE `yi_group_member`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `group_id` int(10) UNSIGNED NULL DEFAULT NULL,
  `user_id` int(10) UNSIGNED NULL DEFAULT NULL,
  `user_status` tinyint(1) UNSIGNED NULL DEFAULT 0 COMMENT '用户成员状态：0普通成员，1管理员，2群主',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `group_id`(`group_id`, `user_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 27 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '群组成员' ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of yi_group_member
-- ----------------------------
INSERT INTO `yi_group_member` VALUES (1, 10000, 100048, 2);
INSERT INTO `yi_group_member` VALUES (25, 10002, 100048, 0);
INSERT INTO `yi_group_member` VALUES (24, 10001, 100048, 0);
INSERT INTO `yi_group_member` VALUES (23, 10000, 100044, 0);
INSERT INTO `yi_group_member` VALUES (26, 10000, 100047, 0);

-- ----------------------------
-- Table structure for yi_system_message
-- ----------------------------
DROP TABLE IF EXISTS `yi_system_message`;
CREATE TABLE `yi_system_message`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL COMMENT '接收用户id',
  `from_id` int(10) UNSIGNED NULL DEFAULT NULL COMMENT '来源相关用户id',
  `is_group` tinyint(1) UNSIGNED NULL DEFAULT 0 COMMENT '0好友 1群',
  `group_id` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '组id或群id',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '添加好友附言',
  `type` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0好友群请求 1请求结果通知',
  `status` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0未处理 1同意 2拒绝 3过期',
  `read` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0未读 1已读，用来显示消息盒子数量',
  `time` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 64 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统消息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yi_system_message
-- ----------------------------
INSERT INTO `yi_system_message` VALUES (60, 100048, 100047, 1, 10000, NULL, 0, 1, 1, 1699434025);
INSERT INTO `yi_system_message` VALUES (59, 100044, 100048, 0, 10000, '同意你的请求', 1, 1, 1, 1699432637);
INSERT INTO `yi_system_message` VALUES (58, 100044, 100048, 1, 10000, '同意你的请求', 1, 1, 1, 1699432619);
INSERT INTO `yi_system_message` VALUES (57, 100048, 100044, 1, 10000, NULL, 0, 1, 1, 1699432431);
INSERT INTO `yi_system_message` VALUES (56, 100048, 100044, 0, 10000, '对对对', 0, 1, 1, 1699432226);
INSERT INTO `yi_system_message` VALUES (55, 100044, 100048, 0, 0, '拒绝你的请求', 1, 2, 1, 1699431976);
INSERT INTO `yi_system_message` VALUES (54, 100048, 100044, 0, 0, NULL, 0, 2, 1, 1699431956);
INSERT INTO `yi_system_message` VALUES (51, 100048, 100047, 0, 0, NULL, 0, 1, 1, 1699431855);
INSERT INTO `yi_system_message` VALUES (52, 100047, 100048, 0, 0, '同意你的请求', 1, 1, 1, 1699431864);
INSERT INTO `yi_system_message` VALUES (53, 100048, 100045, 0, 0, NULL, 0, 0, 1, 1699431936);
INSERT INTO `yi_system_message` VALUES (63, 100047, 100048, 1, 10000, '同意你的请求', 1, 1, 1, 1699434070);
INSERT INTO `yi_system_message` VALUES (62, 100048, 100047, 1, 10000, NULL, 0, 1, 1, 1699434056);
INSERT INTO `yi_system_message` VALUES (61, 100047, 100048, 1, 10000, '同意你的请求', 1, 1, 1, 1699434036);
INSERT INTO `yi_system_message` VALUES (50, 100048, 100001, 0, 0, '同意你的请求', 1, 1, 1, 1699430887);
INSERT INTO `yi_system_message` VALUES (47, 100000, 100048, 0, 0, NULL, 0, 1, 1, 1699430806);
INSERT INTO `yi_system_message` VALUES (48, 100048, 100000, 0, 0, '同意你的请求', 1, 1, 1, 1699430815);
INSERT INTO `yi_system_message` VALUES (49, 100001, 100048, 0, 0, NULL, 0, 1, 1, 1699430873);

-- ----------------------------
-- Table structure for yi_user
-- ----------------------------
DROP TABLE IF EXISTS `yi_user`;
CREATE TABLE `yi_user`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `username` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `avatar` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `sign` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `status` tinyint(1) NULL DEFAULT 0 COMMENT '0离线，1在线，2隐身',
  `fd` int(10) UNSIGNED NULL DEFAULT 0,
  `birthday_year` smallint(5) UNSIGNED NULL DEFAULT NULL,
  `city` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `sex` enum('男','女') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 100049 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yi_user
-- ----------------------------
INSERT INTO `yi_user` VALUES (100000, '温皇', '/upload/header/h1.jpg', '回忆迷惘杀戮多，往事情仇待如何', 1, 0, NULL, NULL, NULL);
INSERT INTO `yi_user` VALUES (100001, '赤羽', '/upload/header/h2.jpg', '恶魔无间来，人魂欲胆寒，天理终循环，生杀吾自在。', 1, 0, NULL, NULL, NULL);
INSERT INTO `yi_user` VALUES (100002, '剑无极', '/upload/header/h3.jpg', '风满楼，卷黄沙，舞剑春秋，名震天下。', 1, 0, NULL, NULL, NULL);
INSERT INTO `yi_user` VALUES (100003, '俏如来', '/upload/header/h4.jpg', '今生何晓几危安，血洒臣虏无间，苍狼敢与天争立，孤掌中，握刀誓斩！', 0, 0, NULL, NULL, NULL);
INSERT INTO `yi_user` VALUES (100004, '风铃一刀声', '/upload/header/h5.jpg', '谈笑风云一杯酒，千金一刃泯恩仇，独饮西楼酆都月，书剑 一叶一江秋。', 0, 0, NULL, NULL, NULL);
INSERT INTO `yi_user` VALUES (100005, '落拓子', '/upload/header/h26.jpg', '傲笑天地间，黑白两不分；马车幽灵影，潇洒一郎君！', 0, 0, NULL, NULL, NULL);
INSERT INTO `yi_user` VALUES (100006, '别小楼', '/upload/header/h6.jpg', '隐身令天下，现身戮万军。', 0, 0, NULL, NULL, NULL);
INSERT INTO `yi_user` VALUES (100007, '慕容烟雨', '/upload/header/h7.jpg', '关公骁勇震神州，麦城一败命难留。', 0, 0, NULL, NULL, NULL);
INSERT INTO `yi_user` VALUES (100008, '诸葛穷', '/upload/header/h8.jpg', '恨天问情心如锁，一念之间愿堕魔。', 0, 0, NULL, NULL, NULL);
INSERT INTO `yi_user` VALUES (100009, '慕容勝雪', '/upload/header/h9.jpg', '踏烽火，折兵锋，正邪无用；斩敌颅，杀魍魉，天地不容', 0, 0, NULL, NULL, NULL);
INSERT INTO `yi_user` VALUES (100010, '丁凌霜', '/upload/header/h10.jpg', '天堂地狱一道门，道门无非三朵云。', 0, 0, NULL, NULL, NULL);
INSERT INTO `yi_user` VALUES (100011, '随风起', '/upload/header/h11.jpg', '云中难觅五形气，气化心逢七彩君。', 0, 0, NULL, NULL, NULL);
INSERT INTO `yi_user` VALUES (100012, '绝命司', '/upload/header/h12.jpg', '红尘轮回众生顾，因果循环有定数', 0, 0, NULL, NULL, NULL);
INSERT INTO `yi_user` VALUES (100013, '步天踪', '/upload/header/h13.jpg', '放下屠刀虽成佛，愿坠三途灭千魔。', 0, 0, NULL, NULL, NULL);
INSERT INTO `yi_user` VALUES (100014, '步清云', '/upload/header/h14.jpg', '我若为魔，魔世天下。我若为人，人间魔土。我若为王，天下尊皇（', 0, 0, NULL, NULL, NULL);
INSERT INTO `yi_user` VALUES (100015, '任孤沉', '/upload/header/h15.jpg', '九天银丝线，八卦罗网长；飞跃地狱门，邪郎掌无常。', 0, 0, NULL, NULL, NULL);
INSERT INTO `yi_user` VALUES (100016, '殷若微', '/upload/header/h16.jpg', '人称一流刀一流，刀称一流人一流。', 0, 0, NULL, NULL, NULL);
INSERT INTO `yi_user` VALUES (100017, '慕容宁', '/upload/header/h17.jpg', '雪花伴孤云，山白不知春，银庄蜘蛛恨，燕城无情君。', 0, 0, NULL, NULL, NULL);
INSERT INTO `yi_user` VALUES (100018, '覆秋霜', '/upload/header/h18.jpg', '无极剑，剑无极，剑招三式， 称无敌。', 0, 0, NULL, NULL, NULL);
INSERT INTO `yi_user` VALUES (100019, '李剑诗', '/upload/header/h19.jpg', '无极剑，剑无极，招招残，敌无命。', 0, 0, NULL, NULL, NULL);
INSERT INTO `yi_user` VALUES (100020, '岳灵休', '/upload/header/h20.jpg', '回首纵横第六天，非神非佛非圣贤，夺命毁法虽本性，身属魔罗心向仙。', 1, 0, NULL, NULL, NULL);
INSERT INTO `yi_user` VALUES (100021, '安倍博雅', '/upload/header/h21.jpg', '萧无名、曲无名、声幽幽、声悲鸣，心何闷？情何困？眉深锁、孤独行。', 0, 0, NULL, NULL, NULL);
INSERT INTO `yi_user` VALUES (100022, '北冥华', '/upload/header/h22.jpg', '憾天无道，唯吾嚣狂，逆宇掩宙，再创神荒。', 0, 0, NULL, NULL, NULL);
INSERT INTO `yi_user` VALUES (100023, '北冥皇渊', '/upload/header/h23.jpg', '北龙归心号苍穹，竞曰风云山河；辕门策令战骁驰，尽下一步干戈。', 0, 0, NULL, NULL, NULL);
INSERT INTO `yi_user` VALUES (100024, '北冥缜', '/upload/header/h24.jpg', '霓霞羽战火连天，墨影神誓护千年。寄语孤鸿诛邪灭，止戈剑印荡魔渊。', 1, 0, NULL, NULL, NULL);
INSERT INTO `yi_user` VALUES (100025, '北冥异', '/upload/header/h25.jpg', '白雪临刃血如泓，百里苍茫独千秋。', 0, 0, NULL, NULL, NULL);
INSERT INTO `yi_user` VALUES (100026, '风间久护', '/upload/header/h27.jpg', '若问明珠还君时，潇湘夜雨寄魂舟。', 0, 0, NULL, NULL, NULL);
INSERT INTO `yi_user` VALUES (100027, '立花雷藏', '/upload/header/h28.jpg', '言语在句君识否，朽木琴雕听无弦。', 0, 0, NULL, NULL, NULL);
INSERT INTO `yi_user` VALUES (100028, '天宫伊织', '/upload/header/h29.jpg', '世何曾分黑白？庸贤石上覆苍苔。', 0, 0, NULL, NULL, NULL);
INSERT INTO `yi_user` VALUES (100029, '月牙诚', '/upload/header/h30.jpg', '骑鹿走崎路，路不平，鹿颠行，人心难安定。', 0, 0, NULL, NULL, NULL);
INSERT INTO `yi_user` VALUES (100030, '未珊瑚', '/upload/header/h31.jpg', '行天涯，扇风雅，独倚晚沙，叹剑无瑕。', 0, 0, NULL, NULL, NULL);
INSERT INTO `yi_user` VALUES (100031, '竞日孤鸣', '/upload/header/h32.jpg', '独向苍天开冷眼，笑问岁月几时休。', 0, 0, NULL, NULL, NULL);
INSERT INTO `yi_user` VALUES (100032, '应龙师', '/upload/header/h33.jpg', '身似秋水任飘渺，名剑求瑕亦多愁。', 0, 0, NULL, NULL, NULL);
INSERT INTO `yi_user` VALUES (100033, '西经无缺', '/upload/header/h34.jpg', '北龙归去没苍穹，长眠银川卧星河。', 0, 0, NULL, NULL, NULL);
INSERT INTO `yi_user` VALUES (100034, '藏镜人', '/upload/header/h35.jpg', '华门月宫悲愁影，尽写一夜长恨歌。', 0, 0, NULL, NULL, NULL);
INSERT INTO `yi_user` VALUES (100035, '狷螭狂', '/upload/header/h36.jpg', '芳菲阑珊，夙缘鶗鴃，风驷云轩愁誓约', 0, 0, NULL, NULL, NULL);
INSERT INTO `yi_user` VALUES (100036, '御兵韬', '/upload/header/h37.jpg', '夜蝶飞阶，霎微雨阙，剑锋无情人葬月。', 0, 0, NULL, NULL, NULL);
INSERT INTO `yi_user` VALUES (100037, '北冥觞', '/upload/header/h38.jpg', '挥笔点墨卷再开，醉仰观岚景悠哉。', 0, 0, NULL, NULL, NULL);
INSERT INTO `yi_user` VALUES (100038, '飞渊', '/upload/header/h39.jpg', '倾向兰曰敬邀曰，叹矣自笑一字呆。', 0, 0, NULL, NULL, NULL);
INSERT INTO `yi_user` VALUES (100039, '荻花题叶', '/upload/header/h40.jpg', '观星望斗惯幽居，一片神鳞渡太虚。', 0, 0, NULL, NULL, NULL);
INSERT INTO `yi_user` VALUES (100040, '川凉剑夫', '/upload/header/h1.jpg', '骑鹿走崎路，路不平，鹿颠行，人心难安定。', 0, 0, NULL, NULL, NULL);
INSERT INTO `yi_user` VALUES (100041, '大忍戒怒', '/upload/header/h3.jpg', NULL, 0, 0, NULL, NULL, NULL);
INSERT INTO `yi_user` VALUES (100042, '了情师太', '/upload/header/h31.jpg', NULL, 0, 0, NULL, NULL, NULL);
INSERT INTO `yi_user` VALUES (100043, '权妃', '/upload/header/h5.jpg', NULL, 0, 0, NULL, NULL, NULL);
INSERT INTO `yi_user` VALUES (100044, '空劫', '/upload/header/h5.jpg', NULL, 1, 0, NULL, NULL, NULL);
INSERT INTO `yi_user` VALUES (100045, '寒夜残灯', '/upload/header/h2.jpg', NULL, 1, 0, NULL, NULL, NULL);
INSERT INTO `yi_user` VALUES (100047, '红目枝', '/upload/header/h40.jpg', NULL, 1, 4, NULL, NULL, NULL);
INSERT INTO `yi_user` VALUES (100048, '道尊', '/upload/header/h36.jpg', '烦烦烦烦烦烦烦烦烦', 1, 98, NULL, NULL, NULL);

SET FOREIGN_KEY_CHECKS = 1;
