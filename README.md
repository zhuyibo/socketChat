# socketChat

#### 介绍
使用swoole搭建简易聊天室，使用workerman搭建类QQ即使通讯

[![Php Version](https://img.shields.io/badge/php-%3E=7.1-brightgreen.svg?maxAge=2592000)](https://secure.php.net/)
[![Swoole Version](https://img.shields.io/badge/swoole-%3E=4.3.3-brightgreen.svg?maxAge=2592000)](https://github.com/swoole/swoole-src)
[![sl-im License](https://img.shields.io/hexpm/l/plug.svg?maxAge=2592000)](https://github.com/gaobinzhan/sl-im/blob/master/LICENSE)


## 简介
1. swoole搭建即时聊天室，支持群聊、私聊，上下线通知，流式打印。
2. layim + worerkman实现的即时聊天，支持群聊、私聊、发表情、发图片、发文件、历史消息、离线消息、消息提醒、在线状态设置、好友分组展示、换肤支持除ie6/ie7以外所有浏览器后端基于workerman，支持万人在线，支持集群部署。


## 功能
- 登录注册（Http）
- 单点登录（Websocket）
- 私聊（Websocket）
- 群聊（Websocket）
- 在线人数（Websocket）
- 获取未读消息（Websocket）
- 好友在线状态（Websocket）
- 好友 查找 添加 同意 拒绝（Http+Websocket）
- 群 创建 查找 添加 同意 拒绝（Http+Websocket）
- 聊天记录存储
- 心跳检测
- 消息重发
- 断线重连

## Requirement

- [PHP 7.1+](https://github.com/php/php-src/releases)
- [Swoole 4.3.4+](https://github.com/swoole/swoole-src/releases)
- [Composer](https://getcomposer.org/)
- [Swoft >= 2.0.8](https://github.com/swoft-cloud/swoft/releases/tag/v2.0.8)



## 部署方式

### Composer

```bash
composer update
```
### bean

`app/bean.php`


```bash
'db' => [
        'class'    => Database::class,
        'dsn'      => 'mysql:dbname=im;host=127.0.0.1:3306',
        'username' => 'root',
        'password' => 'gaobinzhan',
        'charset'  => 'utf8mb4',
    ],
'db.pool' => [
        'class'     => \Swoft\Db\Pool::class,
        'database'  => bean('db'),
        'minActive' => 5, // 自己调下连接池大小
        'maxActive' => 10
    ],
```

### 数据表迁移

` php bin/swoft mig:up`

### env配置

`vim .env`

```bash
# basic
APP_DEBUG=0
SWOFT_DEBUG=0

# more ...
APP_HOST=https://im.gaobinzhan.com/
WS_URL=ws://im.gaobinzhan.com/im
# 是否开启静态处理 这里我关了 让nginx去处理
ENABLE_STATIC_HANDLER=false 
# swoole v4.4.0以下版本, 此处必须为绝对路径
DOCUMENT_ROOT=/data/wwwroot/IM/public
```
### nginx配置

```bash
server{
    listen 80;
    server_name im.gaobinzhan.com;
    return 301 https://$server_name$request_uri;
}

server{
    listen 443 ssl;
    root /data/wwwroot/IM/public/;
    add_header Strict-Transport-Security "max-age=31536000";
    server_name im.gaobinzhan.com;
    access_log /data/wwwlog/im-gaobinzhan-com.access.log;
    error_log /data/wwwlog/im-gaobinzhan-com.error.log;
    client_max_body_size 100m;
    ssl_certificate /etc/nginx/ssl/full_chain.pem;
    ssl_certificate_key /etc/nginx/ssl/private.key;
    ssl_session_timeout 5m;
    ssl_protocols TLSv1.1 TLSv1.2 TLSv1.3;
    ssl_ciphers ECDHE-RSA-AES128-GCM-SHA256:HIGH:!aNULL:!MD5:!RC4:!DHE;
    location / {
        proxy_pass http://127.0.0.1:9091;
        proxy_set_header Host $host:$server_port;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Real-PORT $remote_port;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    }
    location /im {
        proxy_pass http://127.0.0.1:9091;
        proxy_http_version 1.1;
        proxy_read_timeout   3600s;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";
    }
    location ~ .*\.(js|ico|css|ttf|woff|woff2|png|jpg|jpeg|svg|gif|htm)$ {
        root /data/wwwroot/IM/public;
    }
}
```

### Start

- 挂起

```bash
php bin/swoft ws:start
```

- 守护进程化

```bash
php bin/swoft ws:start -d
```
