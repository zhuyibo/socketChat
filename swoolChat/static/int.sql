CREATE TABLE `chat_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `fd` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '推送ID',
  `user_name` varchar(50) NOT NULL DEFAULT '' COMMENT '用户名',
  `avatar` varchar(100) NOT NULL DEFAULT '' COMMENT '头像',
  `online_status` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '在线状态：0离线，1在线',
  `add_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '注册时间',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `user_name` (`user_name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

CREATE TABLE `chat_content` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `user_name` varchar(60) DEFAULT NULL,
  `to_id` int(10) unsigned DEFAULT '0',
  `to_name` varchar(60) DEFAULT NULL,
  `content` text,
  `create_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;